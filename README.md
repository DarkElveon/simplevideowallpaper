# SimpleVideoWallpaper

Simple seamless video wallpaper for KDE Plasma

This project is a fork from the project https://github.com/halverneus/org.kde.video.
Many thanks to the original author.

Unfortunately the loop behavior of Qt generates black 'reloading' frames between the loops.
This fork is aimed to resolve this problem and provide a real seamless loop.

## Installation

```shell
mkdir -p $HOME/.local/share/plasma/wallpapers
cd $HOME/.local/share/plasma/wallpapers
git clone https://gitlab.com/DarkElveon/simplevideowallpaper.git org.kde.video
```

## Changes

To resolve the problem with black 'reloading' frames the `seek()` method is used to jump to the beginning of the video.
So there is no need to close the old video stream and open a new one.

To trigger the `seek()` method we need to know the current position in the video ('position').
Fortunately, since KDE Plasma 5.9 there is an option to set the update interval of the current position (default were 1000 milliseconds).
The interval is now set to 1 millisecond for maximal precision.

This project is dedicated to seamless loop videos. So there is **no** playlist option. This option was removed.

## Procedure

1. The duration of the video is used to calculate the offset. (The offset is used to trigger the `seek()` method)
2. For every subsequent loop the pointer of the player seeks to the beginning of the video. So there are no black frames anymore.
3. In some cases the initial offset is not enough to trigger the `seek()` method. In this cases the offset will be increased until the `seek()` method executed correctly.
4. To avoid incorrect initial offsets the last valid offset for the video is saved in local storage and loaded the next time.
