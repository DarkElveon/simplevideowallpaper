import QtQuick 2.5
import QtMultimedia 5.9
import QtQuick.LocalStorage 2.0
import org.kde.plasma.core 2.0 as Plasmacore

Item {

    MediaPlayer {

        property int max: Math.pow(1024, 3)
        property int trigger: max
        property int offset: 0
        property bool init: true
        property var db: LocalStorage.openDatabaseSync("VideoLoopDB", "1.0", "", 1000000);

        function updateOffset(path, offset) {
            db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM Offset WHERE path = ?', [ path ]);

                    if (rs.rows.length === 0) {
                        tx.executeSql('INSERT INTO Offset VALUES(?, ?)', [ path, offset ]);
                    } else {
                        tx.executeSql('UPDATE Offset SET offset = ? WHERE path = ?', [ offset, path ])
                    }
                }
            )
        }

        function getOffset(path) {
            db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM Offset WHERE path = ?', [ path ]);

                    if (rs.rows.length !== 0) {
                        offset = rs.rows[0].offset
                    } else {
                        offset = 0
                    }
                }
            )
        }

        function initDb() {
            db.transaction(
                function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS Offset(path TEXT, offset NUMBER)');
                }
            )
        }

        id: mediaplayer
        autoPlay: true
        muted: wallpaper.configuration.Muted
        playbackRate: wallpaper.configuration.Rate
        source: wallpaper.configuration.Video

        /* ----------------------------------------------- *
         |  Fix for black frames (since QtMultimedia 5.9)  |
         * ----------------------------------------------- */

        notifyInterval: 1

        onPositionChanged: {
            if (position > trigger) {
                mediaplayer.seek(0)
            }
        }

        onStopped: {
            mediaplayer.play()

            if (!init) {
                var tmp = Math.round(offset / 5)
                offset += tmp > 0 ? tmp : 1
                if (db) updateOffset(source, offset)
                trigger = max - offset
            }
        }

        onDurationChanged: {
            if (duration !== -1) {
                max = duration
                if (offset === 0) {
                    offset = duration / 1000
                    if (db) updateOffset(source, offset)
                }
                trigger = max - offset
                init = false
            }
        }

        onSourceChanged: {
            if (db) getOffset(source)
            init = true
        }
    }

    VideoOutput {
        fillMode: VideoOutput.PreserveAspectCrop
        anchors.fill: parent
        source: mediaplayer

        Component.onCompleted: {
            if (mediaplayer.db) mediaplayer.initDb()
        }
    }
}
